
data "aws_ami" "my_centos_ami" {
  most_recent = true
  owners      = ["679593333241"]

  filter {
   name = "name"
   values = ["CentOS-7-2111-20220825_1.x86_64-d9a3032a-921c-4c6d-b150-bde168105e42"]
  }
}

resource "aws_instance" "romaric-ec2" {
  ami               = data.aws_ami.my_centos_ami.id
  instance_type     = var.instance_type
  key_name          = var.ssh_key
  availability_zone = var.AZ
  security_groups   = ["${var.sg_name}"]
  tags = {
    Name = "${var.maintainer}-ec2"
  }

  root_block_device {
    delete_on_termination = true
  }

  provisioner "local-exec" {
    command = "echo PUBLIC IP: ${var.public_ip} >> IP_ec2.txt"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo yum -y update",
      "curl -fsSL https://get.docker.com -o get-docker.sh",
      "sh get-docker.sh",
      "sudo usermod -aG docker centos",
      "sudo systemctl enable docker",
      "sudo systemctl start docker",
      "sudo yum install git -y",
      "sudo curl -L https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose",
      "sudo chmod +x /usr/local/bin/docker-compose"
    ]
    connection {
      type        = "ssh"
      user        = var.user
      # private_key = file("C:/Users/monji/Downloads/projekt_terraform_1/app/${var.ssh_key}.pem")
      private_key = file("./${var.ssh_key}.pem")
      host        = self.public_ip
    }
  }

}

